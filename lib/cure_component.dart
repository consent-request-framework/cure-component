library cure_component;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/consent.dart';
import 'models/data.dart';
import 'models/purpose.dart';
import 'providers/consent_list_provider.dart';
import 'providers/cure_provider.dart';
import 'providers/purpose_list_provider.dart';
import 'screens/consent/consent_list_screen.dart';
import 'screens/not_found_screen.dart';
import 'screens/purpose/purpose_list_screen.dart';

class CureComponent extends StatelessWidget {
  final Function(int consentId, List<int> purposeIds) onClose;
  final List<Consent> consents;
  final List<Purpose> purposes;
  final List<Data> data;
  final List<Data> activities;
  final List<Data> processors;

  const CureComponent({
    Key key,
    @required this.onClose,
    @required this.consents,
    @required this.purposes,
    @required this.data,
    @required this.activities,
    @required this.processors,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: CureProvider(onClose)),
        ChangeNotifierProvider.value(value: ConsentListProvider(consents)),
        ChangeNotifierProvider.value(value: PurposeListProvider(purposes, data, activities, processors)),
      ],
      child: Navigator(
        onGenerateRoute: (RouteSettings settings) {
          return MaterialPageRoute(
            settings: settings,
            builder: (BuildContext context) {
              switch (settings.name) {
                case ConsentListScreen.routeName:
                  return ConsentListScreen();
                case PurposeListScreen.routeName:
                  return PurposeListScreen();
              }
              return NotFoundScreen(settings.name);
            },
          );
        },
      ),
    );
  }
}
