class Consent {
  final int id;
  final String name;
  final String description;
  final List<int> purposeIds;
  final bool isSelected;
  final bool isCustomisable;
  final bool isUsageBased;
  final int orderIndex;

  Consent({
    this.id,
    this.name,
    this.description,
    this.purposeIds,
    this.isSelected,
    this.isCustomisable,
    this.isUsageBased,
    this.orderIndex,
  });

  Consent copyWith({
    id = Symbol.empty,
    name = Symbol.empty,
    description = Symbol.empty,
    isSelected = Symbol.empty,
    isCustomisable = Symbol.empty,
    isUsageBased = Symbol.empty,
    orderIndex = Symbol.empty,
    purposeIds = Symbol.empty,
  }) {
    return Consent(
      id: id != Symbol.empty ? id : this.id,
      name: name != Symbol.empty ? name : this.name,
      description: description != Symbol.empty ? description : this.description,
      isSelected: isSelected != Symbol.empty ? isSelected : this.isSelected,
      isCustomisable:
          isCustomisable != Symbol.empty ? isCustomisable : this.isCustomisable,
      isUsageBased:
          isUsageBased != Symbol.empty ? isUsageBased : this.isUsageBased,
      orderIndex: orderIndex != Symbol.empty ? orderIndex : this.orderIndex,
      purposeIds: purposeIds != Symbol.empty ? purposeIds : this.purposeIds,
    );
  }

  static fromMap(Map<String, dynamic> data) => Consent(
        id: data['id'],
        name: data['name'],
        description: data['description'],
        orderIndex: data['orderIndex'],
        isSelected: data['isSelected'],
        isCustomisable: data['isCustomisable'],
        isUsageBased: data['isUsageBased'],
        purposeIds: data['purposeIds'].map<int>((x) => x as int).toList(),
      );
}
