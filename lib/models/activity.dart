class Activity {
  final String processor;
  final Map<String, List<String>> processorActivity;

  Activity({
    this.processor,
    this.processorActivity,
  });

  static List<Activity> fromMap(Map<String, dynamic> data) {
    final activities = List<Activity>();
    data.forEach((key, value) {
      if (key == 'processor') {
        final processors = value.map<String>((x) => x.toString()).toList();
        processors.forEach((processor) {
          activities.add(Activity(processor: processor, processorActivity: Map()));
        });
      }
    });

    data.forEach((key, value) {
      if (key != 'processor') {
        final data = value.map<String>((x) => x.toString()).toList();
        activities.forEach((activity) {
          activity.processorActivity[key] = data;
        });
      }
    });

    return activities;
  }

}
