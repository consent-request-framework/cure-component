import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Data {
  final String id;
  final IconData icon;
  final String label;

  Data({
    this.id,
    this.icon,
    this.label,
  });

  Data copyWith({
    id = Symbol.empty,
    icon = Symbol.empty,
    label = Symbol.empty,
  }) {
    return Data(
      id: id != Symbol.empty ? id : this.id,
      icon: icon != Symbol.empty ? icon : this.icon,
      label: label != Symbol.empty ? label : this.label,
    );
  }

  static fromMap(Map<String, dynamic> data) => Data(
        id: data['id'],
        icon: data.containsKey('icon')
            ? IconDataSolid(int.parse('0x${data['icon']}'))
            : null,
        label: data['label'],
      );
}
