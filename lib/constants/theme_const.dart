import 'package:flutter/material.dart';

const colorDarkRed = Color(0xFFE22028);
const colorGold = Color(0xFFFE9644);
const colorGreen = Color(0xFF10AE0C);

const mainButtonTextStyle = TextStyle(color: colorDarkRed, fontSize: 18);
const buttonTheme = ButtonThemeData(
  height: 41,
  shape: StadiumBorder(side: BorderSide(color: colorDarkRed)),
  buttonColor: Colors.white,
);
