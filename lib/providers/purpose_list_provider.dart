import 'dart:core';

import 'package:cure_component/models/consent.dart';
import 'package:cure_component/models/data.dart';
import 'package:cure_component/models/purpose.dart';
import 'package:cure_component/widgets/modal_dialog.dart';
import 'package:flutter/material.dart';

class PurposeListProvider with ChangeNotifier {
  List<Purpose> _purposes = [];
  List<Data> _data = [];
  List<Data> _activities = [];
  List<Data> _processors = [];

  PurposeListProvider(
    List<Purpose> purposes,
    List<Data> data,
    List<Data> activities,
    List<Data> processors,
  ) {
    _purposes = [];
    for (final item in purposes) {
      _purposes.add(item.copyWith());
    }

    _data = [];
    for (final item in data) {
      _data.add(item.copyWith());
    }

    _activities = [];
    for (final item in activities) {
      _activities.add(item.copyWith());
    }

    _processors = [];
    for (final item in processors) {
      _processors.add(item.copyWith());
    }
  }

  List<Purpose> getItems(Consent consent) {
    return _purposes
        .where((x) => consent.purposeIds.contains(x.id))
        .map((x) => x.copyWith(isSelectable: consent.isCustomisable))
        .toList();
  }

  List<Purpose> getPurposes() {
    return [..._purposes];
  }

  List<Data> getData() {
    return [..._data];
  }

  List<Data> getActivities() {
    return [..._activities];
  }

  Data getProcessorById(String id) {
    return _processors.singleWhere((x) => x.id == id).copyWith();
  }

  Future<void> update(context, int purposeId, bool isSelected) async {
    Set<int> siblingIds = {};
    (isSelected)
        ? _findAllParentIds(purposeId, siblingIds, null)
        : _findAllChildIds(purposeId, siblingIds);
    siblingIds.remove(purposeId);

    List<int> selectionIds = _purposes
        .where((x) => x.isSelected == isSelected)
        .map((e) => e.id)
        .toList();

    siblingIds.removeAll(selectionIds);

    final siblings = _findPurposesByIds(siblingIds.toList());
    siblings.sort((a, b) => a.id.compareTo(b.id));

    var areYouSure = true;
    if (siblings.length > 0) {
      areYouSure = await _areYouSure(context, siblings, isSelected);
    }

    if (!areYouSure) {
      return;
    }

    List<int> purposesToUpdate = siblings.map((e) => e.id).toList();
    purposesToUpdate.add(purposeId);
    for (var i = 0; i < _purposes.length; i++) {
      final purpose = _purposes[i];
      if (purposesToUpdate.contains(purpose.id)) {
        _purposes[i] = purpose.copyWith(isSelected: isSelected);
      }
    }

    notifyListeners();
  }

  Future<bool> _areYouSure(
    context,
    List<Purpose> purposeRefs,
    bool isSelected,
  ) {
    final selected = isSelected ? 'selected' : 'deselected';
    return ModalDialog.showAreYouSure(
      context,
      body: RichText(
        textAlign: TextAlign.left,
        text: TextSpan(
          style: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 16.0,
            color: Colors.black,
          ),
          children: <TextSpan>[
            TextSpan(text: 'The following purposes also will be $selected:\n'),
            TextSpan(
              text: purposeRefs.map((e) => e.name).join('\n'),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      yesBtn: isSelected ? 'Select' : 'Deselect',
      noBtn: 'Cancel',
    );
  }
  void _findAllParentIds(int currentId, Set<int> res, List<Purpose> purposes) {
    res.add(currentId);
    final purpose =
        (purposes ?? _purposes).firstWhere((x) => x.id == currentId);
    for (final parentId in purpose.parentIds) {
      if (res.contains(parentId)) {
        continue;
      }
      _findAllParentIds(parentId, res, purposes);
    }
  }

  void _findAllChildIds(int currentId, Set<int> result) {
    result.add(currentId);
    final purpose = _purposes.firstWhere((x) => x.id == currentId);
    final childrenIds = _findChildren(purpose.id);
    for (final childId in childrenIds) {
      if (result.contains(childId)) {
        continue;
      }
      _findAllChildIds(childId, result);
    }
  }

  List<int> _findChildren(int purposeId) {
    return _purposes
        .where((x) => x.parentIds.contains(purposeId))
        .map((e) => e.id)
        .toList();
  }

  List<Purpose> _findPurposesByIds(List<int> purposeIds) {
    List<Purpose> result = [];
    for (int purposeId in purposeIds) {
      final purpose = _purposes.firstWhere((x) => x.id == purposeId).copyWith();
      result.add(purpose);
    }
    return result;
  }
}
