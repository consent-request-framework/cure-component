import 'dart:core';
import 'package:cure_component/models/purpose.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'consent_list_provider.dart';
import 'purpose_list_provider.dart';

class CureProvider with ChangeNotifier {
  final Function(int consentId, List<int> purposeIds) onClose;

  CureProvider(this.onClose);

  void close(BuildContext ctx) {
    final consent = Provider.of<ConsentListProvider>(ctx, listen: false);
    final purpose = Provider.of<PurposeListProvider>(ctx, listen: false);

    var selectedConsent = consent.getSelectedConsent();
    List<Purpose> purposes = purpose.getItems(selectedConsent);
    if (selectedConsent.isCustomisable) {
      purposes = purposes.where((e) => e.isSelected).toList();
    }

    this.onClose(selectedConsent.id, purposes.map((e) => e.id).toList());
  }
}
