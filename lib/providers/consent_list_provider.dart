import 'dart:core';

import 'package:cure_component/models/consent.dart';
import 'package:flutter/material.dart';

class ConsentListProvider with ChangeNotifier {
  List<Consent> _consents = [];

  ConsentListProvider(List<Consent> consents){
    _consents = [];
    for (final item in consents) {
      _consents.add(item.copyWith());
    }
  }

  List<Consent> get items {
    return [..._consents];
  }

  bool get hasConsent {
    return _consents.where((element) => element.isSelected).length != 0;
  }

  Consent findById(int id) {
    return _consents.firstWhere((x) => x.id == id);
  }

  Consent getSelectedConsent() {
    return _consents.firstWhere((x) => x.isSelected);
  }

  void selectConsent(int consentId) {
    List<Consent> consents = [];
    for (final item in _consents) {
      consents.add(item.copyWith(isSelected: item.id == consentId));
    }
    _consents = [... consents];
    notifyListeners();
  }
}
