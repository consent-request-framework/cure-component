import 'package:flutter/material.dart';

class GraphModel {
  String name;
  Map<IconData, String> legend;
  List<GraphItem> items;

  GraphModel({this.name, this.legend, this.items});
}

class GraphItem {
  Map<IconData, String> source;
  Set<IconData> connections;
  String target;

  GraphItem({this.source, this.connections, this.target});
}
