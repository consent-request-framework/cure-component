import 'dart:ui' as ui;

import 'package:cure_component/constants/theme_const.dart';
import 'package:flutter/material.dart';

import 'graph.model.dart';

class GraphConfig {
  final GraphModel data;

  final sectionGap = 50.0;
  final arrowLength = 50.0;
  final graphWidth = 320.0;
  final circleRadius = 13.0;
  final fontSize = 14.0;
  final targetBoxHeight = 30.0;
  final strokeWidth = 1.0;
  final strokeColor = colorDarkRed;
  final textColor = Colors.black;
  final iconColor = colorDarkRed;
  final iconSize = 15.0;
  final textTopGap = 8;
  final rectRadius = 10.0;

  GraphConfig(this.data);

  double getHeight() {
    var height = 0.0;
    data.items.forEach((element) {
      height += element.source.length * targetBoxHeight;
      height += arrowLength;
      height += targetBoxHeight;
      height += sectionGap;
    });
    height += getHeaderHeight(data.name) + 76;

    height -= sectionGap;
    return height;
  }

  double getWidth() {
    return graphWidth;
  }

  getHeaderHeight(String text) {
    final paragraphBuilder = ui.ParagraphBuilder(ui.ParagraphStyle())
      ..pushStyle(ui.TextStyle(fontSize: fontSize, fontWeight: FontWeight.bold))
      ..addText(text);
    final paragraph = paragraphBuilder.build()
      ..layout(ui.ParagraphConstraints(width: graphWidth));
    return paragraph
        .computeLineMetrics()
        .map((e) => e.height)
        .reduce((a, b) => a + b);
  }
}
