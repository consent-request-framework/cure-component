import 'package:cure_component/graph/config.dart';
import 'package:flutter/material.dart';

import 'graph.painter.dart';

class Graph extends StatelessWidget {
  final config;

  Graph(this.config);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      size: Size(config.getWidth(), config.getHeight()),
      painter: GraphPainter(config: config),
    );
  }
}
