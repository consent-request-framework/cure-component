import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import 'config.dart';

class GraphPainter extends CustomPainter {
  final GraphConfig config;

  GraphPainter({this.config});

  @override
  void paint(Canvas canvas, Size size) {
    final data = this.config.data;
    final headerHeight =
        _drawHeader(canvas, Offset(0, 0), data.name.toUpperCase());
    var sectionHeight = headerHeight + config.sectionGap - 5;

    _drawLegend(canvas, Offset(0, headerHeight + 14));

    for (var item in data.items) {
      final sourceHeight = item.source.length * config.targetBoxHeight;
      final position = Offset(0, sectionHeight);
      _drawSourceSection(canvas, position, item.source);

      sectionHeight += (sourceHeight +
          config.targetBoxHeight +
          config.sectionGap +
          config.arrowLength);

      final arrowGap = config.graphWidth / item.connections.length;
      final dx = arrowGap / 2;
      final yPosition = position.dy + sourceHeight;
      for (var i = 0; i < item.connections.length; i++) {
        final connection = item.connections.elementAt(i);
        final xPosition = (i + 1) * arrowGap - dx;
        final triangleHeight = _drawArrow(canvas, Offset(xPosition, yPosition));
        _drawCircle(
            canvas,
            Offset(xPosition,
                yPosition + config.arrowLength / 2 - triangleHeight / 2),
            connection);
      }

      final yPositionTarget = yPosition + config.arrowLength;
      _drawTargetSection(canvas, Offset(0, yPositionTarget), item.target);
    }
  }

  @override
  bool shouldRepaint(GraphPainter oldDelegate) {
    return false;
  }

  _drawLegend(Canvas canvas, Offset offset) {
    var counter = 0;
    config.data.legend.forEach((key, value) {
      final posX = offset.dx +
          4 +
          config.graphWidth / config.data.legend.length * counter;
      final pos = Offset(posX, offset.dy + 1);
      _drawIcon(key, canvas, pos, Colors.black);

      final paragraphBuilder = ui.ParagraphBuilder(ui.ParagraphStyle())
        ..pushStyle(ui.TextStyle(
          color: config.textColor,
          fontWeight: FontWeight.bold,
          fontSize: config.fontSize,
        ))
        ..addText(value);
      final constraints = ui.ParagraphConstraints(width: config.graphWidth);
      final paragraph = paragraphBuilder.build();
      paragraph.layout(constraints);
      final position = Offset(posX + 20, offset.dy);
      canvas.drawParagraph(paragraph, position);

      counter++;
    });
  }

  double _drawHeader(Canvas canvas, Offset offset, String text) {
    final pBuilder =
        ui.ParagraphBuilder(ui.ParagraphStyle(textAlign: TextAlign.center))
          ..pushStyle(ui.TextStyle(
              color: config.strokeColor,
              fontSize: config.fontSize,
              fontWeight: FontWeight.bold))
          ..addText(text);

    final paragraph = pBuilder.build()
      ..layout(ui.ParagraphConstraints(width: config.graphWidth));

    canvas.drawParagraph(paragraph, Offset(offset.dx, offset.dy));

    final yPos = config.getHeaderHeight(text) + 5;
    final paint = Paint()
      ..color = Colors.black26
      ..strokeWidth = config.strokeWidth;
    canvas.drawLine(Offset(0, yPos), Offset(config.graphWidth, yPos), paint);

    return yPos;
  }

  _drawSourceSection(Canvas canvas, Offset offset, Map<IconData, String> data) {
    final rect = Rect.fromLTWH(
        0, offset.dy, config.graphWidth, config.targetBoxHeight * data.length);
    final rectRound =
        RRect.fromRectAndRadius(rect, Radius.circular(config.rectRadius));
    final path = new Path()..addRRect(rectRound);

    Paint paintBorder = Paint()
      ..color = config.strokeColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = config.strokeWidth;
    canvas.drawPath(path, paintBorder);

    final paragraphStyle = ui.ParagraphStyle();
    var counter = 0;
    data.forEach((key, value) {
      final textBuilder = ui.ParagraphBuilder(paragraphStyle)
        ..pushStyle(ui.TextStyle(
          color: config.textColor,
          fontSize: config.fontSize,
          fontWeight: FontWeight.bold,
        ))
        ..addText(value);
      final text = textBuilder.build();
      text.layout(ui.ParagraphConstraints(width: config.graphWidth));
      final position = Offset(offset.dx + 35,
          offset.dy + counter * config.targetBoxHeight + config.textTopGap);
      canvas.drawParagraph(text, position);

      _drawIcon(key, canvas, Offset(offset.dx + 10, position.dy + 1),
          config.iconColor);

      counter++;
    });
  }

  _drawTargetSection(Canvas canvas, Offset offset, String text) {
    final rect =
        Rect.fromLTWH(0, offset.dy, config.graphWidth, config.targetBoxHeight);
    final rectRound =
        RRect.fromRectAndRadius(rect, Radius.circular(config.rectRadius));
    final path = new Path()..addRRect(rectRound);

    Paint paint = Paint()
      ..color = config.strokeColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = config.strokeWidth;
    canvas.drawPath(path, paint);

    final textStyle = ui.TextStyle(
      color: config.textColor,
      fontSize: config.fontSize,
      fontWeight: FontWeight.bold,
    );
    final paragraphStyle = ui.ParagraphStyle(
      textDirection: TextDirection.ltr,
      textAlign: TextAlign.center,
    );
    final paragraphBuilder = ui.ParagraphBuilder(paragraphStyle)
      ..pushStyle(textStyle)
      ..addText(text);
    final constraints = ui.ParagraphConstraints(width: config.graphWidth);
    final paragraph = paragraphBuilder.build();
    paragraph.layout(constraints);
    final position = Offset(offset.dx, offset.dy + config.textTopGap);
    canvas.drawParagraph(paragraph, position);
  }

  _drawArrow(Canvas canvas, Offset offset) {
    Paint paint = Paint()
      ..color = config.strokeColor
      ..strokeWidth = config.strokeWidth;

    final p1 = Offset(offset.dx, offset.dy);
    final p2 = Offset(offset.dx, offset.dy + config.arrowLength);
    canvas.drawLine(p1, p2, paint);

    final triangleWidth = 12;
    final triangleHeight = 8;
    final startX = offset.dx - triangleWidth / 2;
    final startY = offset.dy - triangleHeight;
    var path = Path();
    path.moveTo(startX, startY + config.arrowLength);
    path.lineTo(startX + triangleWidth, startY + config.arrowLength);
    path.lineTo(startX + triangleWidth / 2,
        startY + config.arrowLength + triangleHeight);
    path.close();

    canvas.drawPath(path, paint);

    return triangleHeight;
  }

  _drawCircle(Canvas canvas, Offset offset, IconData iconData) {
    final paint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill
      ..strokeWidth = config.strokeWidth;
    canvas.drawCircle(offset, config.circleRadius, paint);

    final paint2 = Paint()
      ..color = config.strokeColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = config.strokeWidth;
    canvas.drawCircle(offset, config.circleRadius, paint2);

    final fonSize = config.iconSize;
    _drawIcon(iconData, canvas, Offset(offset.dx - fonSize / 2, offset.dy - fonSize / 2),
        Colors.black);
  }

  _drawIcon(IconData icon, Canvas canvas, Offset offset, Color color) {
    TextPainter textPainter = TextPainter(textDirection: TextDirection.rtl);
    textPainter.text = TextSpan(
        text: String.fromCharCode(icon.codePoint),
        style: TextStyle(
            color: color,
            fontSize: config.iconSize,
            fontFamily: icon.fontFamily,
            package: icon.fontPackage));
    textPainter.layout();
    textPainter.paint(canvas, offset);
  }
}
