import 'package:cure_component/models/consent.dart';
import 'package:cure_component/providers/consent_list_provider.dart';
import 'package:cure_component/providers/purpose_list_provider.dart';
import 'package:cure_component/widgets/active_button.dart';
import 'package:cure_component/widgets/modal_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'purpose_item.dart';

class PurposeList extends StatelessWidget {
  final Consent consent;

  const PurposeList(this.consent, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    Future<void> _selectTemplate() async {
      var areYouSure = await ModalDialog.showAreYouSure(
        ctx,
        body: Text('Are you sure you want to give consent?'),
        yesBtn: 'Yes, I\'m Sure',
        noBtn: 'Cancel',
      );

      if (areYouSure) {
        final consent = Provider.of<ConsentListProvider>(ctx, listen: false);
        consent.selectConsent(this.consent.id);
        Navigator.of(ctx).pop();
      }
    }

    return Column(
      children: <Widget>[
        Expanded(
          child: Consumer<PurposeListProvider>(
            builder: (ctx, purposePrd, ch) {
              var purposes = purposePrd.getItems(consent);
              if (purposes.length == 0) {
                return Center(
                  child: Text(
                    consent.description,
                    textAlign: TextAlign.center,
                  ),
                );
              }
              return ListView.builder(
                itemCount: purposes.length,
                itemBuilder: (BuildContext ctx, int i) {
                  return PurposeItem(purposes[i]);
                },
              );
            },
          ),
        ),
        consent.isSelected
            ? Container()
            : Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.3),
                      offset: Offset(0.0, -3.0),
                      blurRadius: 2.0,
                      spreadRadius: 0.05,
                    ),
                  ],
                ),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: ActiveButton(
                      label: 'I Agree',
                      onPressed: _selectTemplate,
                    ),
                  ),
                ),
              ),
      ],
    );
  }
}
