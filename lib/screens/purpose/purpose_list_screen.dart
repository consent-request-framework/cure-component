import 'package:cure_component/providers/consent_list_provider.dart';
import 'package:cure_component/screens/purpose/purpose_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PurposeListScreen extends StatelessWidget {
  static const routeName = '/purpose-list-screen';

  @override
  Widget build(BuildContext ctx) {
    final consent = Provider.of<ConsentListProvider>(ctx, listen: false)
        .findById(ModalRoute.of(ctx).settings.arguments);

    return Scaffold(
      appBar: AppBar(
        title: Text(consent.name),
        centerTitle: true,
      ),
      body: PurposeList(consent),
    );
  }
}
