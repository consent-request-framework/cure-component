import 'package:cure_component/constants/theme_const.dart';
import 'package:cure_component/helpers/bottom_sheet_helper.dart';
import 'package:cure_component/models/data.dart';
import 'package:cure_component/models/purpose.dart';
import 'package:cure_component/providers/purpose_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class PurposeItem extends StatelessWidget {
  final Purpose purpose;

  const PurposeItem(
    this.purpose, {
    Key key,
  }) : super(key: key);

  List<Widget> _getDataWidgets(List<Data> icons) {
    List<Widget> result = [];
    var items = [...purpose.getData()];
    items.sort((a, b) => a.compareTo(b));
    for (final data in items) {
      final dataItem = icons.singleWhere((element) => element.id == data);
      result.add(
        Chip(
          label: Text(
            dataItem.label,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          avatar: FaIcon(
            dataItem.icon,
            size: 15,
            color: colorDarkRed,
          ),
          backgroundColor: Colors.transparent,
        ),
      );
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    var purposePrd = Provider.of<PurposeListProvider>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  purpose.isSelectable
                      ? Switch(
                          value: purpose.isSelected,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          activeColor: colorDarkRed,
                          onChanged: (value) {
                            purposePrd.update(context, purpose.id, value);
                          },
                        )
                      : SizedBox(height: 40),
                  Expanded(
                    child: Text(
                      purpose.name.toUpperCase(),
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: InkWell(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      child: ImageIcon(
                        AssetImage(
                          'assets/images/info_icon.png',
                          package: 'cure_component',
                        ),
                        size: 25,
                        color: Colors.black54,
                      ),
                      onTap: () {
                        BottomSheetHelper.showGraph(context, purpose.id);
                      },
                    ),
                  )
                ],
              ),
              Divider(color: Colors.black26),
              SizedBox(height: 5),
              Wrap(
                spacing: 6.0,
                runSpacing: -10.0,
                children: _getDataWidgets(purposePrd.getData()),
              )
            ],
          ),
        ),
      ),
    );
  }
}
