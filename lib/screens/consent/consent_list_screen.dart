import 'package:cure_component/providers/consent_list_provider.dart';
import 'package:cure_component/providers/cure_provider.dart';
import 'package:cure_component/screens/consent/consent_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConsentListScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Consent'),
        centerTitle: true,
        leading: Consumer<ConsentListProvider>(
          builder: (ctx, consentPrd, ch) => consentPrd.hasConsent
              ? IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () => {
                    Provider.of<CureProvider>(ctx, listen: false).close(ctx),
                  },
                )
              : Container(width: 0.0, height: 0.0),
        ),
      ),
      body: Consumer<ConsentListProvider>(
        child: Center(child: Text('No consent data provided')),
        builder: (ctx, data, noConsents) {
          final items = data.items;
          return (items.length != 0) ? ConsentList(items) : noConsents;
        },
      ),
    );
  }
}
