import 'package:cure_component/models/consent.dart';
import 'package:flutter/material.dart';

import 'consent_item.dart';
import '../../widgets/list_header.dart';

class ConsentList extends StatelessWidget {
  final List<Consent> consents;

  const ConsentList(
    this.consents, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (_, __) => SizedBox(height: 4),
      itemCount: consents.length + 1,
      itemBuilder: (ctx, i) {
        return Padding(
          padding: i == 0
              ? const EdgeInsets.only(top: 12, bottom: 20)
              : const EdgeInsets.only(left: 8.0, right: 8.0),
          child: i == 0
              ? Row(
                  children: <Widget>[
                    ListHeader('Please choose your consent:'),
                    Spacer()
                  ],
                )
              : ConsentItem(consent: consents[i - 1]),
        );
      },
    );
  }
}
