import 'package:cure_component/constants/theme_const.dart';
import 'package:cure_component/models/consent.dart';
import 'package:cure_component/screens/purpose/purpose_list_screen.dart';
import 'package:flutter/material.dart';

class ConsentItem extends StatelessWidget {
  final Consent consent;

  const ConsentItem({Key key, @required this.consent}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    Widget _getIcon(Consent consent) {
      return (consent.isSelected)
          ? ImageIcon(
              AssetImage(
                'assets/images/active_icons.png',
                package: 'cure_component',
              ),
              size: 25,
              color: Colors.black54,
            )
          : SizedBox(height: 25);
    }

    return Card(
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      child: InkWell(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        onTap: () => {
          Navigator.pushNamed(
            ctx,
            PurposeListScreen.routeName,
            arguments: consent.id,
          )
        },
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text(
                    consent.name.toUpperCase(),
                    style: TextStyle(
                      color: colorDarkRed,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Spacer(),
                  _getIcon(consent)
                ],
              ),
              Divider(color: Colors.black26),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 4),
                child: Text(consent.description),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
