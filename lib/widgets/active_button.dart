import 'package:cure_component/constants/theme_const.dart';
import 'package:flutter/material.dart';

class ActiveButton extends StatelessWidget {
  final String label;
  final Function() onPressed;

  const ActiveButton({
    Key key,
    final this.onPressed,
    final this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(label, style: mainButtonTextStyle),
      highlightColor: colorDarkRed.withOpacity(0.15),
      splashColor: colorDarkRed.withOpacity(0.5),
      shape: StadiumBorder(side: BorderSide(color: colorDarkRed)),
      color: Colors.white,
      onPressed: onPressed,
    );
  }
}
