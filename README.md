# Component installation

The following should be specified in the `pubspec.yaml` file in the `dependencies` section:
```yaml
  cure_component:
    git:
      url: https://gitlab.com/consent-request-framework/cure-component.git
```

# Component usage
The `CureComponent` widget provides a functionality that helps users to give their consent to personal data processing.

The constructor of the `CureComponent` widget excepts the following parameters:
* `List<Consent> consents`
* `List<Purpose> purposes`
* `List<Data> data`
* `List<Data> activities`
* `List<Data> processors`
* `Function(int consentId, List<int> purposeIds) onClose`

## Parameter description
### `List<Consent> consents`
List of Consent objects
```dart
class Consent {
  final int id;                   // The unique identifier of a consent.
  final String name;              // Consent name.
  final String description;       // Consent description.
  final List<int> purposeIds;     // List of purpose ids.
  final bool isSelected;          // Is consent selected?
  final bool isCustomisable;      // Is consent customisable?
  final int orderIndex;           // Specifies the custom consent order.
}
```
### `List<Purpose> purposes`
List of Purpose objects
```dart
class Purpose {
  final int id;                        // The unique identifier of a purpose.
  final String name;                   // Purpose name. 
  final bool isSelectable;             // Identifies if a purpose is selectable. It's used when a purpose belongs to a customisable consent.
  final bool isSelected;               // Is purpose selected?
  final List<int> parentIds;           // Specifies dependent purposes. It's used for automatic selection or deselection of dependent purposes.
  final List<Activity> activities;     // List of data processing activities.
}
```
#### `List<Activity> activities`
```dart
class Activity {
  final String processor;                             // Data processor name.
  final Map<String, List<String>> processorActivity;  // List of data processing activities per processor.
}
```

### `List<Data> data`
List of processed personal data
```dart
class Data {
  final String id;     // The unique identifier of a data category.
  final IconData icon; // The icon of a data category.
  final String label;  // The name of a data category.
}
```
### `List<Data> activities`
List of data processing activities
```dart
class Data {
  final String id;     // The unique identifier of a data processing activity.
  final IconData icon; // The icon of a data processing activity.
  final String label;  // The name of a data processing activity.
}
```
### `List<Data> processors`
List of data processors
```dart
class Data {
  final String id;     // The unique identifier of a data processor.
  final String label;  // The name of a data processor.
}
```

### `Function(int consentId, List<int> purposeIds) onClose`
Callback function. This function is triggered when a user has given a consent and closed a consent window.
```dart
int consentId;        // Consent id that was selected by a user.
List<int> purposeIds; // Purpose ids that were selected by a user.
```

Creative Commons Attribution-ShareAlike 4.0 International License
